import redis.clients.jedis.JedisPubSub;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class RedisDemoApp {

    public static String chatChannel = "CH1";
    public static String chatHistory = "history";

    public static void main(String[] args){

        RedisResource resource = new RedisResource();

        Scanner sc = new Scanner(System.in);
        System.out.println("------ what is your username? ------");
        String userName = sc.nextLine();
        System.out.println("------ Connected to chat as: " + userName + " ------\n\n");

        List<String> history = resource.listRead(chatHistory, 10);
        Collections.reverse(history);
        history.stream().forEach(s -> {
            System.out.println(s);
        });

        Runnable userInput = new UserInput(resource, userName);
        Thread userInputThread = new Thread(userInput);
        userInputThread.start();

        resource.subscribe(new JedisPubSub() {
            @Override
            public void onMessage(String channel, String message) {
                if(!message.startsWith(userName + ":")){
                    System.out.println(message);
                }
            }
        }, chatChannel);

    }

}
