import java.util.Scanner;

public class UserInput implements Runnable {

    private RedisResource resource;
    private Scanner scanner;

    private String userName;

    public UserInput(RedisResource resource, String name){
        this.resource = resource;
        this.userName = name;
    }

    public void run() {

        scanner = new Scanner(System.in);
        String nextLine = "";

        while(true){
            nextLine = scanner.nextLine();
            processLine(nextLine);
        }
    }

    private void processLine(String line){

        if("exit".equals(line)){
            System.exit(0);
        }

        String msg = userName + ":\t" +  line;
        resource.publish(RedisDemoApp.chatChannel, msg);
        resource.listPush(RedisDemoApp.chatHistory, msg);
        return;

    }

}
