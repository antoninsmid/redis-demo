import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.JedisShardInfo;

import java.util.List;

public class RedisResource {

    private static String server = "myicpc3.ecs.baylor.edu";
    private static int port = 6379;
    private static String pass = "myicpcredispazz";

    static Jedis jedis;
    static JedisShardInfo jedisShardInfo;

    static Jedis jedisChatSubscribe;

    public RedisResource(){
        jedis = initConnection(server, port, pass);
    }

    private Jedis initConnection(String server, int port, String pass) {
        jedisShardInfo = new JedisShardInfo(server, port);
        jedisShardInfo.setPassword(pass);
        Jedis connection = new Jedis(jedisShardInfo);
        connection.connect();
        return connection;
    }


    public String getValue(String key){
        return jedis.get(key);
    }

    public void setValue(String key, String value){
        jedis.set(key, value);
    }

    public List<String> listRead(String key, int count){
        return jedis.lrange(key, 0, count);
    }

    public void listPush(String key, String value){
        String[] msg = {value};
        jedis.lpush(key, msg);
    }

    public void subscribe(JedisPubSub pubSub, String channel){
        jedisChatSubscribe = initConnection(server, port, pass);
        jedisChatSubscribe.subscribe(pubSub, channel);
    }

    public void publish(String channel, String message){
        jedis.publish(channel, message);
    }

}


